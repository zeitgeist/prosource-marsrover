FROM gradle:jdk17-jammy AS BUILD
WORKDIR /usr/app/
COPY . .
RUN gradle test -i
RUN gradle --no-daemon build

CMD ["gradle",  "run", "-q", "--console=plain"]

