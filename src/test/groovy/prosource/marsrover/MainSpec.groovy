package test.groovy.prosource.marsrover

import prosource.marsrover.Main
import spock.lang.Specification

class MainSpec extends Specification {

    void "processUserInput should warn the user if his input is invalid."() {
        given:
        String userInput = "LMX"

        when:
        String output = Main.processUserInput(userInput)

        then:
        output == "ERROR! Instructions contain invalid letters. \nValid letters are 'L', 'R' and 'M'. "
    }

    void "processUserInput should return the correct output if the none of the letters in the instructions are invalid."() {
        given:
        String userInput = "LMLM"

        when:
        String output = Main.processUserInput(userInput)

        then:
        output == "Output: 0, 0, S"
    }
}
