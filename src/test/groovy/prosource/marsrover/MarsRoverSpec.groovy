package test.groovy.prosource.marsrover
import prosource.marsrover.MarsRover

import spock.lang.Specification
import spock.lang.Unroll

class MarsRoverSpec extends Specification {

    void "getPosition() should start with 0,0,N when the mars rover is initialized."() {
        when:
        MarsRover marsRover = new MarsRover()

        then:
        marsRover.getPosition() == "0, 0, N"
    }

    @Unroll
    void "getPosition should return the correct position given the following instructions (input: #input | expected output: #output)."() {
        given:
        MarsRover marsRover = new MarsRover()

        when:
        marsRover.processInstructions(input)

        then:
        marsRover.getPosition() == output

        where:
        input           | output
        "LMLMLMLMLM"    | "0, 1, W"
        "LM"            | "0, 0, W"
        "LMLM"          | "0, 0, S"
        "RM"            | "1, 0, E"
    }

    void "canProcessInstructions should return false if one or more letters in the instructions is invalid."() {
        given:
        MarsRover marsRover = new MarsRover()

        when:
        boolean actualOutput = marsRover.canProcessInstructions(input)

        then:
        actualOutput == false

        where:
        input << [ "LMRX", "MR123", "xyz", "lrm" ]
    }

    void "canProcessInstructions should return true if all letters in the instructions is valid."() {
        given:
        MarsRover marsRover = new MarsRover()

        when:
        boolean actualOutput = marsRover.canProcessInstructions(input)

        then:
        actualOutput == true

        where:
        input << [ "LMR", "MRL", "LMLMLM", "MLM" ]
    }
}
