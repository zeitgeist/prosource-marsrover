package prosource.marsrover;

import java.util.Scanner;

public class Main {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter your instructions: ");

    while(scanner.hasNextLine()) {
      String userInput = scanner.nextLine();

      if(userInput.equals("exit")) {
        break;
      }

      String output = processUserInput(userInput);

      System.out.println(output);
      System.out.println("\n\nEnter your instructions: ");
    }

    System.out.println("Bye!");
    scanner.close();
    System.exit(0);
  }

  public static String processUserInput(String userInput) {
    MarsRover marsRover = new MarsRover();

    if(!marsRover.canProcessInstructions(userInput)) {
      return "ERROR! Instructions contain invalid letters. \nValid letters are 'L', 'R' and 'M'. ";
    }

    marsRover.processInstructions(userInput);
    return "Output: " + marsRover.getPosition();
  }
}
