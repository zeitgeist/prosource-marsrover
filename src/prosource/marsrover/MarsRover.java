package prosource.marsrover;

public class MarsRover {

  private int x;
  private int y;
  private char direction;

  public MarsRover() {
    this.x = 0;
    this.y = 0;
    this.direction = 'N';
  }

  public String getPosition() {
    return x + ", " + y + ", " + direction;
  }

  public boolean canProcessInstructions(String instructions) {
    return instructions.matches("[LRM]+");
  }

  public void processInstructions(String instructions) {
    for(char instruction: instructions.toCharArray()) {
      switch (instruction) {
        case 'L' -> turnLeft();
        case 'R' -> turnRight();
        case 'M' -> moveForward();
      }
    }
  }

  private void moveForward() {
    switch (direction) {
      case 'N' -> y++;
      case 'E' -> x++;
      case 'S' -> {
        int SOUTH_BORDER = 0;
        if (y > SOUTH_BORDER) {
          y--;
        }
      }
      case 'W' -> {
        int WEST_BORDER = 0;
        if (x > WEST_BORDER) {
          x--;
        }
      }
    }
  }

  private void turnRight() {
    switch (direction) {
      case 'N' -> direction = 'E';
      case 'E' -> direction = 'S';
      case 'S' -> direction = 'W';
      case 'W' -> direction = 'N';
    }
  }

  private void turnLeft() {
    switch (direction) {
      case 'N' -> direction = 'W';
      case 'W' -> direction = 'S';
      case 'S' -> direction = 'E';
      case 'E' -> direction = 'N';
    }
  }

}
